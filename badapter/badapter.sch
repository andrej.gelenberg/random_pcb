EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  2350 1400 1050
U 5FE1406E
F0 "LME49740NA_amp" 50
F1 "LME49740NA_amp.sch" 50
$EndSheet
$Sheet
S 700  750  1400 1300
U 60051872
F0 "bayer_adapter" 50
F1 "bayer_adapter.sch" 50
$EndSheet
$Sheet
S 2550 750  750  800 
U 6005AF85
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 2600 2400 1150 1050
U 6005B0B9
F0 "tpa6120" 50
F1 "tpa6120.sch" 50
$EndSheet
$EndSCHEMATC
